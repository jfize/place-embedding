# Work on Place-embedding 

This repo contains various approach around geographic place embedding, and more precisely on its use for geocoding. At this moment, we designed three approaches :

 * Use of geographic places Wikipedia pages to learn an embedding for toponyms
 * Use Geonames place topology to produce an embedding using graph-embedding techniques
 * Use toponym colocation (combination ?) based on spatial relatationships (inclusion, adjacency) for geocoding

<hr>

## Setup environnement

- Python3.6+
- Os free (all dependencies work on Windows !)

It is strongly advised to used Anaconda in a windows environnement! 

### Install dependencies

    pip3 install -r requirements.txt

For Anaconda users

    while read requirement; do conda install --yes $requirement; done < requirements.txt

<hr>

##  Embedding using places Wikipedia pages

<div style="text-align:center">
<img src="documentation/imgs/first_approach.png"/>
<p>Figure 1 : First approach general workflow</p>
</div>

In this first approach, the goal is to produce embedding for place name. In order to do this, we designed a neural network that takes :

* **Input:** Text sequence (phrase)
* **Output** Latitute, Longitude, and the place type

Input texts are selected using Wikidata to filter Wikipedia pages about geographic places. Then, the filtered pages are retrieved on the Wikipedia corpus file. For each pages, we got :

* Title
* Introduction text
* Coordinates of the place (laatitude-Longitude)
* Place type (using a mapping between Wikidata and DBpedia Place subclasses)

### Step 1: Parse Wikipedia data !

First, download the Wikipedia corpus in the wanted language, *e.g. enwiki-latest-pages-articles.xml.bz2*

Then, use the `gensim` parser (doc [here](https://radimrehurek.com/gensim/scripts/segment_wiki.html)). Use the following command :

    python3 -m gensim.scripts.segment_wiki -i -f <wikipedia_dump_file> -o <output>

### Step 2: Select and Filter entity from Wikidata

We use Wikidata to identify which Wikipedia pages concern a place. Simply, run the following command : 

    python3 1_extractDataFromWikidata.py <Wikidata Dump (.gz)> <output_filename>

### Step 3: Extract data from Wikipedia pages

Using previous output, we extract text data from selected Wikipedia pages with the following command:

    python3 2_extractLearningDataset.py <wikipedia_filename (output from step 1)> <wikidata_extract(output from step2)> <output_filename>

### Step 4 : Run Embedding extraction

To learn extract the place embedding, use the `embeddings_lat_lon_type.py`

#### Available Parameters

| Parameter              | Value (default)     |
|------------------------|---------------------|
| --max_sequence_length          | Maximum sequence length (15) |
| --embedding_dimension           | Embedding vector size (100)             |
| --batch_size | batch size used in the training (100)             |
| --epochs         | Number of epochs (100) |
| -v                     | Display the keras verbose          |

#### Output

The different outputs (on for each neural network architecture) are put in the `outputs` directory : 

* outputs/Bi-GRU_100dim_20epoch_1000batch__coord.png : **coordinates accuracy plot**
* outputs/Bi-GRU_100dim_20epoch_1000batch__place_type.png : **place type accuracy plot**
* outputs/Bi-GRU_100dim_20epoch_1000batch.csv : **training history**
* outputs/Bi-GRU_100dim_20epoch_1000batch.txt : **embeddings**
