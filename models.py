from keras import Model
from keras.layers import Input, Dense, Bidirectional, LSTM, Embedding, GRU, GlobalAveragePooling1D

def getModel(model_func,max_sequence_length,embedding_dim,num_words,class_encoder,coordinate_encoder):
    sequence_input = Input(shape=(max_sequence_length,), dtype='int32')
    embedding_layer = Embedding(num_words, embedding_dim,  input_length=max_sequence_length, trainable=True)
    x = embedding_layer(sequence_input)
    
    name,x = model_func(x)

    placetype_layer = Dense(len(class_encoder.classes_), activation='softmax',name="place_type")(x) # From the transformation, attempt to predict the place type
    coordinates_layer = Dense(coordinate_encoder.oneDimensionOutputSize(), activation='softmax',name="coord")(x)

    model = Model(sequence_input, [ placetype_layer , coordinates_layer] )
    model.compile(loss=['categorical_crossentropy','categorical_crossentropy'], optimizer='adam',metrics=["accuracy"])
    return name,model

def WEAverage_model(x):
    name = "WordEmb_Average"
    x = GlobalAveragePooling1D()(x)
    return name,x

def MPC_WEAverage_model(x):
    name = "MPC_WordEmb_Average"
    x = GlobalAveragePooling1D()(x)
    x = Dense(2000)(x)
    x = Dense(2000)(x)
    return name,x

def BI_LSTM_model(x):
    name = "Bi-LSTM"
    x = (Bidirectional(LSTM(64)))(x)
    return name,x

def BI_GRU_model(x):
    name = "Bi-GRU"
    x = (Bidirectional(GRU(64)))(x)
    return name, x
