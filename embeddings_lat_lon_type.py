# Basic module
import json
import argparse

# Data module
import numpy as np
import pandas as pd

# preprocessing
from sklearn import preprocessing
from keras.preprocessing.text import Tokenizer

# Neural network model and visualisation function
from models import getModel,BI_GRU_model, BI_LSTM_model, MPC_WEAverage_model, WEAverage_model
from helpers import plot_accuracy_from_history, save_embedding

# Utils
from utils import CoordinatesEncoder,_split, ConfigurationReader

# Logging
import logging
from helpers import Chronometer
logging.basicConfig(
    format='[%(asctime)s][%(levelname)s] %(message)s ', 
    datefmt='%m/%d/%Y %I:%M:%S %p',
    level=logging.INFO
    )
chrono = Chronometer()

# Visualisation
import matplotlib.pyplot as plt
from tqdm import tqdm

parser = ConfigurationReader(configuration_file="parser_config/embeddings_lat_lon.json")
args = parser.parse_args()

def clean(x):
    return x.lower().replace("\n","").replace("\'\'\'","").replace("\'\'","")

def split_data(input_data):
    """
    Split the corpus into different list, each one corresponding to a feature(coordinates, type, textual data)
    
    Parameters
    ----------
    input_data : _io.TextIOWrapper
        File instance
    
    Returns
    -------
    tuple
        lists of locations : name, coordinates, types and text data
    """
    listLocations, listText, listCoords, listTypes = [], [], [], []
    for line in input_data:
        data = json.loads(line.strip("\n"))
        listLocations.append(data["title"])
        listText.append(clean(data["section_texts"][0]))  # get intro
        # listTypes.append(data["classes"] if data["classes"] else "Q5624766") # Default Populated Places
        listTypes.append(dict_class_translate[data["classes"]]
                         if data["classes"] in dict_class_translate else "populated place")
        listCoords.append([float(data["lat"]), float(data["lon"])])
    return listLocations, listCoords, listTypes, listText



# PARSE ARGS
args = parser.parse_args()#("./WikipediaExtract/WikipediaExtract_filtered_300.json".split())

GLOVE_DIR = args.glove_dir
MAX_SEQUENCE_LENGTH = args.max_sequence_length  # Size of the context window for word2vec CBOW
MAX_NUM_WORDS = args.max_num_words  # Number of words in the vocabulary
EMBEDDING_DIM = args.embedding_dimension  # Dimensionality for the word embeddings
BATCH_SIZE = args.batch_size  # Size of the training batches of instances
EPOCHS = args.epochs
CORPUS_FILENAME = args.input

# SEARCH FOR UNIQUE TYPES
logging.info("Collecting class name")

# For class translation
dict_class_translate = json.load(open("data/wikidata/class_data/WikiClass2DPClass.json"))

classlabels = set([])
for line in tqdm(open(CORPUS_FILENAME), desc="Reading the line "):
    data = json.loads(line.strip("\n"))
    classlabels.add(dict_class_translate[data["classes"]]
                    if data["classes"] in dict_class_translate else "populated place")
    #classlabels.add(data["classes"] if data["classes"] else "Q5624766")
classlabels = list(classlabels)

# LOAD DATA
logging.info("Loading the data...")
chrono.start("data_loading")

_, listCoords, listTypes, listText = split_data(open(CORPUS_FILENAME))

logging.info("Data Loaded in {0} seconds!".format(chrono.stop("data_loading")))

logging.info("Extract Vocab")
# Extract Vocab
vocab_ = set([" "])
tokenizer = Tokenizer()
tokenizer.fit_on_texts(listText)
tokenizer.word_index[" "] = np.max(list(tokenizer.index_word.keys()))+1 
vocab_ = vocab_.union(tokenizer.word_index.keys())
logging.info("The vocabulary contains {0} words".format(len(list(vocab_))))

logging.info("Initialize Tokenizer/ClassEncoder/CoordinateEncoder...")
# Tokenizer
max_key_tokenizer = np.max(list(tokenizer.index_word.keys()))
num_words = min(MAX_NUM_WORDS, len(tokenizer.word_index)) + 1
# Coordinate Encoder
coordinate_encoder = CoordinatesEncoder(2,2)
# CLASS ENCODER type-->int
type_encoder = preprocessing.LabelEncoder()
type_encoder.fit(classlabels)


logging.info("Parsing data for the neural network...")
chrono.start("parse_data")
X = tokenizer.texts_to_sequences(listText)

#X = X[:500] # Sample for tests

listCoords = np.array(listCoords)
y_lat = listCoords[:, 0]
y_lon = listCoords[:, 1]


new_X,Y_type,Y_coord = [],[],[]
for ix,x in tqdm(enumerate(X),total=len(X)):
    text_sequence_splited = _split(x,MAX_SEQUENCE_LENGTH,0)
    
    # Sub-Sequences vectors
    new_X.extend(text_sequence_splited)
    
    # coordinate vectors
    new_coordinates=coordinate_encoder.vector_flatten(y_lat[ix],y_lon[ix])
    Y_coord.extend([new_coordinates]*len(text_sequence_splited))
    
    #type vectors
    type_code = type_encoder.transform([listTypes[ix]])[0]
    arr_ = np.zeros(len(type_encoder.classes_))
    arr_[type_code]=1
    Y_type.extend([arr_]*len(text_sequence_splited))


Y_coord = np.array(Y_coord)
Y_type = np.array(Y_type)
new_X = np.array(new_X)

logging.info("Data Parsed in {0} seconds!".format(chrono.stop("parse_data")))

for model_f in [BI_GRU_model, BI_LSTM_model, MPC_WEAverage_model, WEAverage_model]:
    name, model = getModel(model_f,MAX_SEQUENCE_LENGTH,EMBEDDING_DIM,num_words,type_encoder,coordinate_encoder)
    logging.info("Training the {0} model...".format(name))
    chrono.start("model_training")
    name = name + \
        "_{0}dim_{1}epoch_{2}batch".format(EMBEDDING_DIM, EPOCHS, BATCH_SIZE)

    history = model.fit(x=new_X,
                    y=[Y_type,Y_coord],#Y_lat,Y_lon],
                    validation_split=0.33,
                    epochs=EPOCHS,
                    batch_size=BATCH_SIZE,
                    verbose=(1 if args.v else 0),
                    workers = 4,
                    #use_multiprocessing=True
                    )

    logging.info("Model {0} have been trained in {1} seconds!".format(name,chrono.stop("model_training")))
    hist_df = pd.DataFrame(history.history)
    hist_df.to_csv("outputs/{0}.csv".format(name))
    for layer_name in "place_type coord".split():
        plt.clf()
        plot_accuracy_from_history(
            name, hist_df, layer_name, "outputs/{0}.png".format(name), "")
    save_embedding(model, tokenizer, 0, "outputs/{0}.txt".format(name))


