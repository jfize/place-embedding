# Basic import 
import math
import argparse
import os
import json

# Data Structure
import numpy as np
import geopandas as gpd
from shapely.geometry import Point,box

# NLP 
from nltk.tokenize import word_tokenize
from ngram import NGram

# Machine learning 
from gensim.models import Word2Vec

# Visualisation and parallelisation
from tqdm import tqdm


class TokenizerCustom():
    def __init__(self,vocab):
        self.word_index = {vocab[i]:i for i in range(len(vocab))}
        self.index_word = {i:vocab[i] for i in range(len(vocab))}
        self.N = len(self.index_word)
    def texts_to_sequences(self,listText):
        seqs = []
        for text in listText:
            seqs.append([self.word_index[word] for word in word_tokenize(text) if word in self.word_index])
        return seqs


class CoordinatesEncoder:
    """
    Will be replaced by Grid in grid2.py
    """
    def __init__(self, cell_size_lat=0.5, cell_size_lon=0.5):
        self.min_lon = -180
        self.max_lon = -(self.min_lon)  #  Symetric
        self.min_lat = -90
        self.max_lat = -(self.min_lat)  # Symetric

        self.ecart_lat = self.max_lat - self.min_lat
        self.ecart_lon = self.max_lon - self.min_lon

        self.cell_size_lat = cell_size_lat
        self.cell_size_lon = cell_size_lon

        self.unit_size_lat = self.ecart_lat / self.cell_size_lat
        self.unit_size_lon = self.ecart_lon / self.cell_size_lon

    def encode(self, lat, lon):
        return (
            math.floor(((lat + self.max_lat) / self.ecart_lat) * self.unit_size_lat),
            math.floor(((lon + self.max_lon) / self.ecart_lon) * (self.unit_size_lon))
        )

    def number_lat_cell(self):
        return int(self.unit_size_lat)

    def number_lon_cell(self):
        return int(self.unit_size_lon)

    def oneDimensionOutputSize(self):
        return self.number_lat_cell() * self.number_lon_cell()

    def vector(self, lat, lon):
        lat_v, lon_v = np.zeros(self.number_lat_cell()), np.zeros(self.number_lon_cell())
        new_coords = self.encode(lat, lon)
        lat_v[int(new_coords[0])] = 1
        lon_v[int(new_coords[1])] = 1
        return lat_v, lon_v

    def vector_flatten(self, lat, lon):
        vec = np.zeros(self.oneDimensionOutputSize())  # 2D Dense softmax isn't possible
        new_coords = self.encode(lat, lon)
        pos = self.number_lat_cell() * (new_coords[0]) + new_coords[1]
        vec[pos] = 1  # lon * lon size
        return vec


class NgramIndex():
    """
    Class used for encoding words in ngram representation
    """
    def __init__(self,n):
        """
        Constructor
        
        Parameters
        ----------
        n : int
            ngram size
        """
        self.ngram_gen = NGram(N=n)

        self.size = n
        self.ngram_index = {"":0}
        self.index_ngram = {0:""}
        self.cpt = 0
        self.max_len = 0

    def split_and_add(self,word):
        """
        Split word in multiple ngram and add each one of them to the index
        
        Parameters
        ----------
        word : str
            a word
        """
        ngrams = word.lower().replace(" ","$")
        ngrams = list(self.ngram_gen.split(ngrams))
        [self.add(ngram) for ngram in ngrams]

    def add(self,ngram):
        """
        Add a ngram to the index
        
        Parameters
        ----------
        ngram : str
            ngram
        """
        if not ngram in self.ngram_index:
            self.cpt+=1
            self.ngram_index[ngram]=self.cpt
            self.index_ngram[self.cpt]=ngram

    def encode(self,word):
        """
        Return a ngram representation of a word
        
        Parameters
        ----------
        word : str
            a word
        
        Returns
        -------
        list of int
            listfrom shapely.geometry import Point,box
 of ngram index
        """
        ngrams = word.lower().replace(" ","$")
        ngrams = list(self.ngram_gen.split(ngrams))
        [self.add(ng) for ng in ngrams if not ng in self.ngram_index]
        return [self.ngram_index[ng] for ng in ngrams]

    def complete(self,ngram_encoding,MAX_LEN,filling_item=0):
        """
        Complete a ngram encoded version of word with void ngram. It's necessary for neural network.
        
        Parameters
        ----------
        ngram_encoding : list of int
            first encoding of a word
        MAX_LEN : int
            desired length of the encoding
        filling_item : int, optional
            ngram index you wish to use, by default 0
        
        Returns
        -------
        list of int
            list of ngram index
        """
        assert len(ngram_encoding) <= MAX_LEN
        diff = MAX_LEN - len(ngram_encoding)
        ngram_encoding.extend([filling_item]*diff)  
        return ngram_encoding
    
    def get_embedding_layer(self,texts,dim=100,**kwargs):
        """
        Return an embedding matrix for each ngram using encoded texts. Using gensim.Word2vec model.
        
        Parameters
        ----------
        texts : list of [list of int]
            list of encoded word
        dim : int, optional
            embedding dimension, by default 100
        
        Returns
        -------
        np.array
            embedding matrix
        """
        model = Word2Vec([[str(w) for w in t] for t in texts], size=dim,window=5, min_count=1, workers=4,**kwargs)
        N = len(self.ngram_index)
        embedding_matrix = np.zeros((N,dim))
        for i in range(N):
            embedding_matrix[i] = model.wv[str(i)]
        return embedding_matrix

    def save(self,fn):
        """

        Save the NgramIndex
        
        Parameters
        ----------
        fn : str
            output filename
        """
        data = {
            "ngram_size": self.size,
            "ngram_index": self.ngram_index,
            "cpt_state": self.cpt,
            "max_len_state": self.max_len
        }
        json.dump(data,open(fn,'w'))

    @staticmethod
    def load(fn):
        """
        
        Load a NgramIndex state from a file.
        
        Parameters
        ----------
        fn : str
            input filename
        
        Returns
        -------
        NgramIndex
            ngram index
        
        Raises
        ------
        KeyError
            raised if a required field does not appear in the input file
        """
        try:
            data = json.load(open(fn))
        except json.JSONDecodeError:
            print("Data file must be a JSON")
        for key in ["ngram_size","ngram_index","cpt_state","max_len_state"]:
            if not key in data:
                raise KeyError("{0} field cannot be found in given file".format(key))
        new_obj = NgramIndex(data["ngram_size"])
        new_obj.ngram_index = data["ngram_index"]
        new_obj.index_ngram = {v:k for k,v in new_obj.ngram_index.items()}
        new_obj.cpt = data["cpt_state"]
        new_obj.max_len = data["max_len_state"]
        return new_obj


def zero_one_encoding(long,lat):
    """
    Encode coordinates (WGS84) between 0 and 1
    
    Parameters
    ----------
    long : float
        longitude value
    lat : float
        latitude value
    
    Returns
    -------
    float,float
        longitude, latitude
    """
    return ((long + 180.0 ) / 360.0), ((lat + 90.0 ) / 180.0) 

def _split(lst,n,complete_chunk_value):
    """
    Split a list into chunk of n-size.
    
    Parameters
    ----------
    lst : list
        input list
    n : int
        chunk size
    complete_chunk_value : object
        if last chunk size not equal to n, this value is used to complete it
    
    Returns
    -------
    list
        chunked list
    """
    chunks = [lst[i:i + n] for i in range(0, len(lst), n)]
    if not chunks:return chunks
    if len(chunks[-1]) != n:
        chunks[-1].extend([complete_chunk_value]*(n-len(chunks[-1])))
    return np.array(chunks)

def generate_couple(object_list):
    """
    Return a randomly selected couple from an object list.
    
    Parameters
    ----------
    object_list : list
        object list
    
    Returns
    -------
    list
        list of coupled object
    """
    couples = []
    lst = np.arange(len(object_list))
    for _ in range(len(object_list)):
        if len(lst) == 1:
            break
        idx = np.random.choice(np.arange(len(lst)))
        idx2 = np.random.choice(np.arange(len(lst)))
        while idx2 == idx:
            idx2 = np.random.choice(np.arange(len(lst)))
        couples.append([object_list[lst[idx]],object_list[lst[idx2]]])
        lst = np.delete(lst,idx)
    return couples

def _hash_couple(o1,o2):
    """
    Return an hash for two object ids.
    
    Parameters
    ----------
    o1 : str or int
        id of the first objeeect
    o2 : str of int
        id of the second object
    
    Returns
    -------
    str
        hash
    """
    return "|".join(map(str,sorted([int(o1),int(o2)])))



### GEO ADJAC BEGIN
class Cell(object):
    """
    A cell is box placed in geeographical space.
    """
    def __init__(self,upperleft_x,upperleft_y,bottomright_x,bottomright_y,x,y):
        """
        Constructor
        
        Parameters
        ----------
        object : [type]
            [description]
        upperleft_x : float
            upperleft longitude
        upperleft_y : float
            upperleft latitude
        bottomright_x : float
            bottom right longitude
        bottomright_y : float
            bottom right latitude
        x : int
            cell x coordinates in the grid
        y : int
            cell y coordinates in the grid
        """
        self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y = upperleft_x,upperleft_y,bottomright_x,bottomright_y
        self.box_ = box(self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y)
        self.list_object={} # {id:Point(coord)}

        self.x,self.y = x, y

    def contains(self,lat,lon):
        """
        Return true if the cell contains a point at given coordinates
        
        Parameters
        ----------
        lat : float
            latitude
        lon : float
            longitude
        
        Returns
        -------
        bool
            true if contains
        """ 
        x,y = lon,lat
        if x < self.upperleft_x or x > self.bottomright_x:
            return False
        if y < self.upperleft_y or y > self.bottomright_y:
            return False
        return True
    
    def add_object(self,id_,lat,lon):
        """
        Connect an object to the cell
        
        Parameters
        ----------
        id_ : int
            id
        lat : float
            latitude
        lon : float
            longitude
        """
        self.list_object[id_] = Point(lon,lat)
            
    def __repr__(self):
        return  "upperleft:{0}_{1}_;bottom_right:{2}_{3}".format(self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y)
        
class Grid(object):
    """
    Define a grid 
    
    """
    def __init__(self,upperleft_x,upperleft_y,bottomright_x,bottomright_y,cell_sub_div_index=[100,50]):
        """
        Constructor
        
        Parameters
        ----------
        upperleft_x : float
            upperleft longitude
        upperleft_y : float
            upperleft latitude
        bottomright_x : float
            bottom right longitude
        bottomright_y : float
            bottom right latitude
        cell_sub_div_index : list, optional
            number of division in both latitude and longitude axis (longitude first), by default [100,50]
        """
        self.upperleft_x,self.upperleft_y,self.bottomright_x,self.bottomright_y = upperleft_x,upperleft_y,bottomright_x,bottomright_y
        
        self.x_r = abs(self.bottomright_x - self.upperleft_x)/cell_sub_div_index[0]
        self.y_r = abs(self.upperleft_y - self.bottomright_y )/cell_sub_div_index[1]
        
        self.c_x_r = self.x_r/cell_sub_div_index[0] # Redivide
        self.c_y_r = self.y_r/cell_sub_div_index[1]
        
        self.cells = []
        self.inter_cells = []
        for i in range(cell_sub_div_index[1]):
            self.cells.append([])
            for j in range(cell_sub_div_index[0]):
                self.cells[-1].append(Cell(
                    self.upperleft_x+j*self.x_r,
                    self.upperleft_y+i*self.y_r,
                    self.upperleft_x+((j+1)*self.x_r),
                    self.upperleft_y+((i+1)*self.y_r),
                    j,i)
                )
        dec_y = 0 
        for i in range(cell_sub_div_index[1]):
            self.inter_cells.append([])
            dec_x = 0 
            for j in range(cell_sub_div_index[0]):                 
                self.inter_cells[-1].append(Cell(
                    self.upperleft_x+(j*self.x_r)-self.c_x_r, # TOP
                    self.upperleft_y+(i*self.y_r)-dec_y,
                    self.upperleft_x+((j+1)*self.x_r)-self.c_x_r,#(self.u_pos*self.c_x_r),
                    self.upperleft_y+((i+1)*self.y_r)+self.c_y_r,
                    j,i)
                )
                self.inter_cells[-1].append(Cell(
                    self.upperleft_x+(j*self.x_r)-self.c_x_r, # CENTER
                    self.upperleft_y+(i*self.y_r)-self.c_y_r,
                    self.upperleft_x+((j+1)*self.x_r)+self.c_x_r,
                    self.upperleft_y+((i+1)*self.y_r)+self.c_y_r,
                    j,i)
                )
                self.inter_cells[-1].append(Cell(
                    self.upperleft_x+(j*self.x_r)+dec_x, # CENTER
                    self.upperleft_y+(i*self.y_r)-self.c_y_r,
                    self.upperleft_x+((j+1)*self.x_r)-self.c_x_r, #LEFT
                    self.upperleft_y+((i+1)*self.y_r)+self.c_y_r,
                    j,i)
                )
                dec_x = self.c_x_r
            dec_y = self.c_y_r
    
    def fit_data(self,data = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))):
        """
        
        To avoid unnecessary check when connecting an entity to one or multiple cells, we 
        filter cells that does not appears in our geographic context (here countries surface).
        
        Parameters
        ----------
        data : GeoDataFrame
            geographic context
        """
        world = data 
        world["nn"] = 1
        dissolved = world.dissolve(by="nn").iloc[0].geometry
        new_cells= []
        new_inter_cells=[]
        for i in tqdm(range(len(self.cells))):
            for j in range(len(self.cells[i])):
                if dissolved.intersects(self.cells[i][j].box_):
                    new_cells.append(self.cells[i][j])
                    new_inter_cells.extend(self.inter_cells[i][j*3:(j+1)*3])
                    
        self.cells=new_cells
        self.inter_cells = new_inter_cells
        
                    
    def __add__(self,a): 
        """
        Add an object to the grid
        
        Parameters
        ----------
        a : tuple
            (id, latitude, longitude)
        """
        for c1 in range(len(self.cells)):
            if self.cells[c1].contains(a[1],a[2]):
                self.cells[c1].add_object(*a)
                
        for c1 in range(len(self.inter_cells)):
            if self.inter_cells[c1].contains(a[1],a[2]):
                self.inter_cells[c1].add_object(*a)
                
    def get_adjacent_relationships(self,random_iteration=10):
        """
        Return a list of adjacent relationships founds in each cell.
        
        Parameters
        ----------
        random_iteration : int, optional
            number of iteration for random selection of adjacency relationships, by default 10
        
        Returns
        -------
        list
            adjacency relationships
        """
        relationships = set([])
        for c1 in tqdm(range(len(self.cells))):
            for i in range(random_iteration):
                for t in generate_couple(list(self.cells[c1].list_object.keys())):
                    relationships.add(_hash_couple(t[0],t[1]))

        for c1 in tqdm(range(len(self.inter_cells))):
            for i in range(random_iteration):
                for t in generate_couple(list(self.inter_cells[c1].list_object.keys())):
                    relationships.add(_hash_couple(t[0],t[1]))
        return relationships
    

### GEO ADJAC END

class ConfigurationReader(object):
    def __init__(self,configuration_file):
        if not os.path.exists(configuration_file):
            raise FileNotFoundError("'{0} file could not be found ! '".format(configuration_file))

        self.configuration = json.load(open(configuration_file))

        self.__argparser_desc = ("" if not "description" in self.configuration else self.configuration["description"])
        self.parser = argparse.ArgumentParser(description=self.__argparser_desc)

        self.parse_conf()
    
    def parse_conf(self):
        if not "args" in self.configuration:
            raise argparse.ArgumentError("","No args given in the configuration file")
        
        for dict_args in self.configuration["args"]:
            if not isinstance(dict_args,dict):
                raise ValueError("Args must be dictionnary")

            short_command = dict_args.get("short",None)
            long_command = dict_args.get("long",None)
            
            if not short_command and not long_command:
                raise ValueError("No command name was given !") 
            
            add_func_dict_= {}
            if "help" in dict_args:
                add_func_dict_["help"]= dict_args["help"]
            if "default" in dict_args:
                add_func_dict_["default"]= dict_args["default"]
            if "action" in dict_args:
                add_func_dict_["action"]= dict_args["action"]
            if "type" in dict_args:
                add_func_dict_["type"]= eval(dict_args["type"])
            if "choices" in dict_args:
                add_func_dict_["choices"]= dict_args["choices"]

            if not (short_command and long_command):
                command = (short_command if not long_command else long_command)
                self.parser.add_argument(command,**add_func_dict_)

            elif long_command and short_command:
                self.parser.add_argument(short_command,long_command,**add_func_dict_)
    
    def parse_args(self,input_=None):
        if not input_:
            return self.parser.parse_args()
        return self.parser.parse_args(input_)



if __name__ == "__main__":

    index = NgramIndex(3)
    index.split_and_add("J'aime le paté")
    encoding = index.encode("xxxyyyy")
    index.complete(encoding,10)