import gzip
import json
import argparse

import pandas as pd

from joblib import Parallel,delayed
from tqdm import tqdm

parser = argparse.ArgumentParser()

parser.add_argument("wikipedia_archive_filename",help="Filename of the Wikipedia corpus parsed with gensim")
parser.add_argument("wikidata_extraction",help="Output from the previous step")
parser.add_argument("output_file")

args = parser.parse_args()

try:
    df_extract_wikidata = pd.read_csv(args.wikidata_extraction)#,header=None,names=["ID_WIKIDATA","title","url","latitude","longitude","classes"])
except:
    df_extract_wikidata = pd.read_csv(args.wikidata_extraction,sep="\t")#,header=None,names=["ID_WIKIDATA","title","url","latitude","longitude","classes"])

titles = set(df_extract_wikidata.title.values)

coords_lat = dict(df_extract_wikidata["title latitude".split()].values)
coords_lon = dict(df_extract_wikidata["title longitude".split()].values)
class_ = dict(df_extract_wikidata["title classes".split()].values)

output = open(args.output_file,'w')




def job(line):
    line = line.decode("utf-8")
    data = json.loads(line)
    if data["title"] in titles:
        title = data["title"]
        data["lat"] = coords_lat[title]
        data["lon"] = coords_lon[title]
        data["classes"] = class_[title]
        output.write(json.dumps(data)+"\n")
        
Parallel(n_jobs = 8,backend="multiprocessing")(delayed(job)(line) for line in tqdm(gzip.GzipFile(args.wikipedia_archive_filename,'rb'),total=5980533))